import org.jetbrains.kotlin.gradle.plugin.PLUGIN_CLASSPATH_CONFIGURATION_NAME

plugins {
    kotlin("jvm") version "1.9.0"
    application
}

group = "com.colorata.jetwaita"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))

    compileOnly("org.jetbrains:annotations:24.+")
    implementation("com.github.jwharm.java-gi:glib:latest.release")
    implementation("com.github.jwharm.java-gi:gtk:latest.release")
    implementation("com.github.jwharm.java-gi:adwaita:latest.release")

    api("org.jetbrains.compose.runtime:runtime:1.4.0")
    add(PLUGIN_CLASSPATH_CONFIGURATION_NAME, "org.jetbrains.compose.compiler:compiler:1.5.0")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(20)
}

application {
    mainClass.set("MainKt")
}

tasks.withType(JavaCompile::class.java) {
    options.compilerArgs = listOf("--enable-preview")
}

tasks.withType(JavaExec::class.java) {
    jvmArgs(
        "--enable-preview",
        "--enable-native-access=ALL-UNNAMED",
        "-Djava.library.path=/usr/lib64:/lib64:/lib:/usr/lib:/lib/x86_64-linux-gnu"
    )
}