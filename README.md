# Jetwaita

This is a framework to get **native** UI with [Jetpack Compose](https://developer.android.com/jetpack/compose/) and [Libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/).

## Quick Demo

![](imagery/quick_demo.webm)

Code:

```kotlin
fun main() = runJetwaita(
    "com.colorata.jetwaita"
) {
    var count by remember { mutableStateOf(0) }
    Column(
        horizontalAlignment = Alignment.Center,
        verticalAlignment = Alignment.Center
    ) {
        Button(onClick = { if (count < 10) count += 1 }, label = "+")

        Label(count.toString())

        Button(onClick = { count -= 1 }, label = "-")

        if (count == 10) Label("You reached maximum")
    }
}
```

## Advanced demo

![](imagery/advanced_demo.webm)

Code:

```kotlin
fun main() = runJetwaita(
    "com.colorata.jetwaita"
) {
    val window = LocalWindow.current
    var count by remember { mutableStateOf(0) }
    Column(
        horizontalAlignment = Alignment.Fill,
        verticalSpacing = 12
    ) {
        HeaderBar(end = {
            Button(onClick = {
                window.showAboutDialog(
                    "Jetwaita Demo",
                    developers = listOf("Colorata"),
                    designers = listOf("Colorata"),
                    version = "1.0.0",
                    issueUrl = "https://gitlab.com/Colorata/Jetwaita/issues",
                    translatorCredit = "translator-credits",
                    licenseType = License.GPL_3_0_ONLY
                )
            }, label = "Info")
        }) {
            WindowTitle("Jetwaita", subtitle = "Demo")
        }
        Row(
            horizontalSpacing = 12,
            horizontalAlignment = Alignment.Center
        ) {
            Button(
                onClick = { if (count < 10) count += 1 },
                label = "+",
                buttonStyle = ButtonStyle.Pill
            )
            Label(count.toString())
            Button(
                onClick = { count -= 1 },
                label = "-",
                buttonStyle = ButtonStyle.Pill
            )
        }
        if (count == 10) Label("You reached maximum")
    }
}
```

## Build & Run
Not available in any library repository yet.

To build this project, clone it and make sure your gradle is running with **Java 20**.

Because this library is based on [Java-GI](https://github.com/jwharm/java-gi), you will need to install some
dependencies. More info [here](https://jwharm.github.io/java-gi/).

To run, just type `./gradlew run` in terminal.

## TODO

- [ ] Proper Modifier system
- [ ] Modifier system stabilization
- [ ] AdwaitaNode stabilization
- [ ] Publishing on Maven

### Gtk widgets:
- [x] Label
- [ ] Spinner
- [ ] Level Bar
- [ ] Progress Bar
- [ ] Scrollbar
- [ ] Image
- [x] Text (Partially)
- [ ] Separators
- [ ] Scales
- [ ] GL area
- [ ] Drawing Area
- [ ] Video
- [ ] Media controls
- [ ] Window Controls
- [ ] Menu bar
- [ ] Calendar
- [ ] Status bar
- [ ] Emoji Chooser
- [x] Button
- [ ] Switches
- [x] Box


### Adwaita widgets:
- [x] Status Page
- [x] Toast overlay
- [x] Banner
- [x] Avatar
- [x] Message dialog
- [x] Action row
- [ ] Combo row
- [x] Expander row
- [x] Entry row
- [x] Password entry row
- [x] Preferences Group
- [x] Preferences Page
- [x] Preferences Window
- [x] About Window
- [ ] Carousel
- [x] View Switcher
- [ ] Tabs
- [ ] Clamp
- [ ] Leaflet
- [ ] Flap
- [ ] Squeezer
- [x] Window Title
- [x] Header Bar
- [x] Window
- [ ] Split Button
- [ ] Button Content
- [ ] Bin