import com.colorata.jetwaita.node.core.Change
import com.colorata.jetwaita.node.core.calculateDiffWithoutInsertion
import org.junit.jupiter.api.Test
import kotlin.test.assertContentEquals

class CalculateDiffNoInsertionTest {

    @Test
    fun `Insert in center`() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf("1", "2", "5", "3", "4")
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Remove("4"))
                add(Change.Remove("3"))
                add(Change.Add("5"))
                add(Change.Add("3"))
                add(Change.Add("4"))
            },
            diff
        )
    }

    @Test
    fun `Insert in start`() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf("5", "1", "2", "3", "4")
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Remove("4"))
                add(Change.Remove("3"))
                add(Change.Remove("2"))
                add(Change.Remove("1"))
                add(Change.Add("5"))
                add(Change.Add("1"))
                add(Change.Add("2"))
                add(Change.Add("3"))
                add(Change.Add("4"))
            },
            diff
        )
    }

    @Test
    fun `Insert in end`() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf("1", "2", "3", "4", "5")
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Add("5"))
            },
            diff
        )
    }

    @Test
    fun Delete() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf("1", "3", "4")
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Remove("4"))
                add(Change.Remove("3"))
                add(Change.Remove("2"))
                add(Change.Add("3"))
                add(Change.Add("4"))
            },
            diff
        )
    }

    @Test
    fun `Delete all`() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf<String>()
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Remove("4"))
                add(Change.Remove("3"))
                add(Change.Remove("2"))
                add(Change.Remove("1"))
            },
            diff
        )
    }

    @Test
    fun `Add all`() {
        val old = listOf<String>()
        val new = listOf("1", "2", "3", "4")
        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(
            buildList {
                add(Change.Add("1"))
                add(Change.Add("2"))
                add(Change.Add("3"))
                add(Change.Add("4"))
            },
            diff
        )
    }

    @Test
    fun `No diff`() {
        val old = listOf("1", "2", "3", "4")
        val new = listOf("1", "2", "3", "4")

        val diff = calculateDiffWithoutInsertion(old, new)

        assertContentEquals(buildList {  }, diff)
    }

}