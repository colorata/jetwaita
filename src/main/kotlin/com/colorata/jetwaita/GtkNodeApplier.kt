package com.colorata.jetwaita

import androidx.compose.runtime.AbstractApplier
import com.colorata.jetwaita.node.core.GtkNode

class GtkNodeApplier(root: GtkNode<*>) : AbstractApplier<GtkNode<*>>(root) {
    override fun insertTopDown(index: Int, instance: GtkNode<*>) {
        current.children.add(index, instance)
    }

    override fun insertBottomUp(index: Int, instance: GtkNode<*>) {
        // Ignored as the tree is built top-down.
    }

    override fun remove(index: Int, count: Int) {
        current.children.remove(index, count)
    }

    override fun move(from: Int, to: Int, count: Int) {
        current.children.move(from, to, count)
    }

    override fun onClear() {
        root.children.clear()
    }
}