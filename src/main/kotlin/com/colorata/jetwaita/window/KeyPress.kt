package com.colorata.jetwaita.window

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import org.gnome.gdk.ModifierType
import org.gnome.gtk.EventControllerKey

@Composable
fun KeyPressHandler(block: (keyValue: Int, keycode: Int, state: ModifierType?) -> Boolean) {
    val controller = remember {
        EventControllerKey().apply {
            onKeyPressed { keyValue, keycode, state ->
                block(keyValue, keycode, state)
            }
        }
    }
    val window = LocalWindow.current
    DisposableEffect(Unit) {
        window.gtkWindow.addController(controller)

        onDispose {
            window.gtkWindow.removeController(controller)
        }
    }
}

@Composable
fun KeyPressHandler(block: (keyValue: Int) -> Unit) {
    KeyPressHandler { keyvalue, keycode, state ->
        block(keyvalue)
        true
    }
}