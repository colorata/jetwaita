package com.colorata.jetwaita.window

import androidx.compose.runtime.compositionLocalOf
import org.gnome.adw.Adw
import org.gnome.gtk.License

interface Window {
    val gtkWindow: org.gnome.gtk.Window
}

fun Window.showAboutDialog(
    appName: String,
    version: String? = null,
    copyright: String? = null,
    issueUrl: String? = null,
    licenseType: License? = null,
    translatorCredit: String? = null,
    developers: List<String>? = null,
    designers: List<String>? = null
) =
    Adw.showAboutWindow(
        gtkWindow,
        "application-name", appName,
        "version", version,
        "copyright", copyright,
        "issue-url", issueUrl,
        "license-type", licenseType,
        "translator-credits", translatorCredit,
        "developers", arrayOf(*developers?.toTypedArray() ?: arrayOf(), null),
        "designers", arrayOf(*designers?.toTypedArray() ?: arrayOf(), null),
        null
    )

fun Window(gtkWindow: org.gnome.gtk.Window) = object : Window {
    override val gtkWindow = gtkWindow
}

val LocalWindow = compositionLocalOf<Window> { error("No window is provided!") }