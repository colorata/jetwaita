package com.colorata.jetwaita.window

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainCoroutineDispatcher
import kotlinx.coroutines.Runnable
import org.gnome.glib.GLib
import kotlin.coroutines.CoroutineContext

/**
 * Should be used when executing Gtk calls (e.g. showing window).
 */
@Suppress("UnusedReceiverParameter")
val Dispatchers.GtkMain: MainCoroutineDispatcher
    get() = GtkMainDispatcher()

private class GtkMainDispatcher : MainCoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        GLib.idleAddFull(1) {
            block.run()
            false
        }
    }

    override val immediate: MainCoroutineDispatcher
        get() = this
}