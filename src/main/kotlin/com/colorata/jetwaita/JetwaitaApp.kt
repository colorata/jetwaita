package com.colorata.jetwaita

import org.gnome.adw.Application
import org.gnome.gio.ApplicationFlags

class JetwaitaApp(
    appId: String, flags: ApplicationFlags,
    onActivate: Application.() -> Unit = {}
) :
    Application(appId, flags) {

    init {
        onActivate { onActivate() }
        run(arrayOf())
    }

    override fun run(argv: Array<out String?>?): Int {
        return super.run(argv)
    }
}