package com.colorata.jetwaita

@RequiresOptIn(message = "This api is not intended to be used directly, please use another way")
annotation class InternalJetwaitaApi