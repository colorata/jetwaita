package com.colorata.jetwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember

data class JetwaitaConfiguration(
    val suppressNotSingleChildren: Boolean = false
)

fun JetwaitaConfiguration.requireSingleChild(childCount: Int, semantics: String) {
    if (!suppressNotSingleChildren) {
        require(childCount == 1) {
            """
                Required 1 child in $semantics, but got $childCount children.
                Please use only 1 child.
                """.trimIndent()
        }
    }
}

@Composable
inline fun <T> rememberWithConfiguration(crossinline block: (JetwaitaConfiguration) -> T): T {
    val configuration = LocalConfiguration.current
    return remember(configuration) { block(configuration) }
}

val LocalConfiguration = compositionLocalOf<JetwaitaConfiguration> { error("No Configuration Provided") }