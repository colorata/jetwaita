package com.colorata.jetwaita

import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.Snapshot
import com.colorata.jetwaita.foundation.Size
import com.colorata.jetwaita.node.ApplicationWindowNode
import com.colorata.jetwaita.window.LocalWindow
import com.colorata.jetwaita.window.Window
import kotlinx.coroutines.*
import org.gnome.gdk.Display
import org.gnome.gio.ApplicationFlags
import org.gnome.gtk.CssProvider
import org.gnome.gtk.Gtk
import org.gnome.gtk.StyleContext

fun runJetwaita(
    appId: String,
    stylePath: String? = null,
    configuration: JetwaitaConfiguration = JetwaitaConfiguration(),
    content: @Composable () -> Unit
) {
    JetwaitaApp(appId, ApplicationFlags.DEFAULT_FLAGS) {
        val rootNode = ApplicationWindowNode(this, configuration).apply {
            size = Size(400, 400)
            title = "Hello"
        }
        if (stylePath != null) loadCss(stylePath)
        CoroutineScope(Dispatchers.Default).launch {
            val frameClock = BroadcastFrameClock()

            val effectCoroutineContext = coroutineContext + frameClock

            Snapshot.registerGlobalWriteObserver {
                Snapshot.sendApplyNotifications()
            }

            val recomposer = Recomposer(effectCoroutineContext)
            val composition = Composition(applier = GtkNodeApplier(rootNode), parent = recomposer)
            composition.setContent {
                activeWindow?.let {
                    val window by remember(it) { mutableStateOf(Window(it)) }
                    CompositionLocalProvider(
                        LocalWindow provides window,
                        LocalConfiguration provides configuration
                    ) {
                        content()
                    }
                }
            }

            launch(context = frameClock, start = CoroutineStart.UNDISPATCHED) {
                recomposer.runRecomposeAndApplyChanges()
            }

            launch {
                while (true) {
                    frameClock.sendFrame(System.nanoTime())
                    delay(16)
                }
            }
            launch {
                recomposer.currentState.collect {
                    rootNode.renderSafe()
                }
            }
        }
    }
}

private fun loadCss(stylePath: String) {
    val provider = CssProvider()
    provider.loadFromPath(stylePath)
    StyleContext.addProviderForDisplay(
        Display.getDefault(),
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )
}