package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.modifier.cssClass
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.gtk.Button
import org.gnome.gtk.Widget

enum class ButtonStyle(val cssName: String = "") {
    Regular,
    Flat("flat"),
    Suggested("suggested-action"),
    Destructive("destructive-action"),
    Pill("pill")
}

@Composable
fun Button(
    onClick: () -> Unit,
    label: String,
    modifier: Modifier = Modifier,
    buttonStyle: ButtonStyle = ButtonStyle.Regular
) {
    ComposeGtkNode(
        modifier = modifier.cssClass(buttonStyle.cssName),
        factory = { ButtonNode() }
    ) {
        set(onClick) {
            this.onClick = it
        }
        set(label) { widget.label = it }
    }
}

class ButtonNode : AdwaitaNode<Button>() {
    override val widget: Button by lazy {
        Button.newWithLabel("")
    }

    var onClick = {}

    override fun create(children: List<Widget>) {
        widget.onClicked(onClick)
    }
}