package com.colorata.jetwaita.foundation

import org.gnome.gtk.Align

enum class Alignment {
    Start,
    Center,
    End,
    Fill,
    Baseline
}

fun Alignment.toGtkAlignment(): Align {
    return when (this) {
        Alignment.Start -> Align.START
        Alignment.Center -> Align.CENTER
        Alignment.End -> Align.END
        Alignment.Fill -> Align.FILL
        Alignment.Baseline -> Align.BASELINE
    }
}