package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.Updater
import androidx.compose.runtime.remember
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.LocalConfiguration
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.GtkNode
import com.colorata.jetwaita.window.GtkMain
import com.colorata.jetwaita.window.LocalWindow
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.gnome.gtk.Widget
import org.gnome.gtk.Window

enum class WindowAction {
    Close,
    Retain
}

@Composable
fun <T : GtkNode<out K>, K: Window> ComposeWindowNode(
    modifier: Modifier = Modifier,
    factory: (parent: Window, configuration: JetwaitaConfiguration) -> T,
    update: Updater<T>.() -> Unit,
    onExit: () -> WindowAction,
    content: @Composable () -> Unit
) {
    val configuration = LocalConfiguration.current
    val parent = LocalWindow.current
    val node = remember {
        factory(parent.gtkWindow, configuration).apply {
            widget.onCloseRequest {
                onExit() == WindowAction.Retain
            }
        }
    }
    DisposableEffect(Unit) {
        runBlocking(Dispatchers.GtkMain) {
            node.widget.present()
        }
        onDispose {
            runBlocking(Dispatchers.GtkMain) {
                node.widget.close()
            }
        }
    }
    ComposeGtkNode(modifier, factory = { node }, update = update, content = content)
}