package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.gtk.Label
import org.gnome.gtk.Widget

@Composable
fun Label(label: String, modifier: Modifier = Modifier) {
    ComposeGtkNode(modifier, factory = { LabelNode() }) {
        set(label) { widget.label = it }
    }
}

class LabelNode: AdwaitaNode<Label>() {
    override val widget = Label("")
}