package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.modifier.semantics
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.gtk.Label
import org.gnome.gtk.Widget

@Composable
fun Slot(content: @Composable () -> Unit) {
    ComposeGtkNode(factory = { SlotNode() }, update = {}) { content() }
}

@Composable
fun Slot(name: String, content: @Composable () -> Unit) {
    ComposeGtkNode(modifier = Modifier.semantics(name), factory = { SlotNode() }, update = {}) { content() }
}

private val emptyModifier by lazy { Label("") }

class SlotNode : AdwaitaNode<Widget>() {
    override val widget = emptyModifier

    override val includeInTree: Boolean = false
}