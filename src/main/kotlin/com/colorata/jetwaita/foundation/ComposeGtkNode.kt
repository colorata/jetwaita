package com.colorata.jetwaita.foundation

import androidx.compose.runtime.*
import com.colorata.jetwaita.GtkNodeApplier
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.LocalConfiguration
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.GtkNode
import kotlinx.coroutines.launch

@Composable
fun <T : GtkNode<*>> ComposeGtkNode(
    modifier: Modifier = Modifier,
    factory: (configuration: JetwaitaConfiguration) -> T,
    update: Updater<T>.() -> Unit
) {
    val configuration = LocalConfiguration.current
    val node = remember { factory(configuration) }
    materialize(node, modifier)
    ComposeNode<T, GtkNodeApplier>(factory = { node }, update = {
        update()
    })
}

@Composable
fun <T : GtkNode<*>> ComposeGtkNode(
    modifier: Modifier = Modifier,
    factory: (configuration: JetwaitaConfiguration) -> T,
    update: Updater<T>.() -> Unit,
    content: @Composable () -> Unit
) {
    val configuration = LocalConfiguration.current
    val node = remember { factory(configuration) }
    materialize(node, modifier)
    ComposeNode<T, GtkNodeApplier>(factory = { node }, update = {
        update()
    }, content = content)
}

@Composable
private fun materialize(node: GtkNode<*>, modifier: Modifier) {
    LaunchedEffect(Unit) {
        modifier.observe().collect { block ->
            block(node)
        }
    }
}