package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.Avatar

@Composable
fun Avatar(
    size: Int,
    modifier: Modifier = Modifier,
    text: String = "",
    showInitials: Boolean = true,
    iconName: String = ""
) {
    ComposeGtkNode(modifier, factory = { AvatarNode() }, update = {
        set(size) { widget.size = it }
        set(text) { widget.text = it }
        set(showInitials) { widget.showInitials = it }
        set(iconName) { widget.iconName = it }
    })
}

class AvatarNode : AdwaitaNode<Avatar>() {
    override val widget = Avatar(
        /* size = */ 100,
        /* text = */ "H",
        /* showInitials = */ true
    )
}