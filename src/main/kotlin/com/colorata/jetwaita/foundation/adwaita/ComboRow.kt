package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import com.colorata.jetwaita.window.GtkMain
import io.github.jwharm.javagi.util.ListIndexModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.gnome.adw.ComboRow
import org.gnome.adw.EnumListModel
import org.gnome.gio.ListModel
import org.gnome.gtk.Button
import org.gnome.gtk.Label
import org.gnome.gtk.ListItem
import org.gnome.gtk.SignalListItemFactory
import org.gnome.gtk.StringList
import org.gnome.gtk.Widget


/**
 * Does not work properly yet
 */
@Composable
internal fun ComboRow(modifier: Modifier = Modifier, showDropDown: Boolean = false, content: @Composable () -> Unit) {
    val node = remember { ComboRowNode() }
    LaunchedEffect(showDropDown) {
        /*launch(Dispatchers.GtkMain) {
            delay(1000)
            node.widget.activate()
        }*/
    }
    ComposeGtkNode(modifier, factory = { node }, update = {}, content = content)
}

/**
 * Does not work properly yet
 */
internal class ComboRowNode : AdwaitaNode<ComboRow>() {
    override val widget = ComboRow().apply {
        model = StringList(arrayOf("H", "E"))
        selected = 0
        title = "HEllo"
        factory = SignalListItemFactory().apply {
            onSetup {
                val item = it as ListItem
                item.child = Label("Hello")
            }
        }
        factory = SignalListItemFactory().apply {
            onSetup {
                val item = it as ListItem
                item.child = Label("Hello")
            }
        }
        val activa = { activate() }
        addPrefix(Button().apply {
            onClicked { activa() }
        })
    }

    override fun create(children: List<Widget>) {
        /*val factory = SignalListItemFactory()
        var index = 0
        factory.onSetup {
            val item = it as ListItem
            if (index < children.size) {
                item.child = children[index]
                index += 1
            }
        }*/
        val model = ListIndexModel.newInstance(children.size)
        /*widget.model = model
        widget.factory = factory*/
        /*println("Creating")
        widget.model = StringList(arrayOf("He", "llo"))
        widget.selected = 0
        widget.title = "HEllo"*/
        /*widget.selected = 0
        widget.listFactory = factory*/
    }
}