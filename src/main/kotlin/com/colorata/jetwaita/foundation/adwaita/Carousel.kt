package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.IndexSiblingLayoutApplier
import com.colorata.jetwaita.node.core.LayoutApplier
import com.colorata.jetwaita.node.core.LayoutNode
import org.gnome.adw.Carousel
import org.gnome.gtk.Widget

interface CarouselState {
    val carousel: Carousel
}

private class CarouselStateImpl: CarouselState {
    override val carousel = Carousel()
}

@Composable
fun rememberCarouselState(): CarouselState {
    return remember { CarouselStateImpl() }
}

@Composable
fun Carousel(state: CarouselState, modifier: Modifier = Modifier, content: @Composable () -> Unit) {
    ComposeGtkNode(modifier, factory = { CarouselNode(state) }, update = {}) {
        content()
    }
}

class CarouselNode(state: CarouselState): LayoutNode<Carousel>() {
    override val widget = state.carousel
    override val layoutApplier = widget.asLayoutApplier()
}

fun Carousel.asLayoutApplier() = object : IndexSiblingLayoutApplier {
    override fun insertChildAfter(widget: Widget, sibling: Int) {
        this@asLayoutApplier.insert(widget, sibling)
    }

    override fun append(widget: Widget) {
        this@asLayoutApplier.append(widget)
    }

    override fun remove(widget: Widget) {
        this@asLayoutApplier.remove(widget)
    }

}