package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slotWithClass
import com.colorata.jetwaita.node.core.slots
import org.gnome.adw.PreferencesGroup
import org.gnome.adw.PreferencesPage
import org.gnome.gtk.Widget
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@Composable
fun PreferencesPage(modifier: Modifier = Modifier, content: @Composable () -> Unit) {
    ComposeGtkNode(modifier, factory = ::PreferencesPageNode, update = {}) {
        Slot("Content", content)
    }
}

class PreferencesPageNode(configuration: JetwaitaConfiguration) : SlotLayoutNode<PreferencesPage>(configuration) {
    // TODO: Why page.title and page.iconName needed?
    override val widget = PreferencesPage()

    override val slots = slots {
        slotWithClass<PreferencesGroup>(onRemove = { widget.remove(it) }) {
            widget.add(it)
        }
    }
}