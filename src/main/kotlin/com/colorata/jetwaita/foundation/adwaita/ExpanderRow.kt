package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.*
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.*
import org.gnome.adw.ExpanderRow

@Composable
fun ExpanderRow(
    title: String,
    modifier: Modifier = Modifier,
    subtitle: String = "",
    expanded: Boolean = false,
    prefix: @Composable () -> Unit = {},
    actions: @Composable () -> Unit = {},
    content: @Composable () -> Unit
) {
    ComposeGtkNode(modifier, factory = ::ExpanderRowNode, update = {
        set(expanded) { widget.expanded = it }
        set(title) { widget.title = it }
        set(subtitle) { widget.subtitle = it }
    }) {
        Slot("Prefix", prefix)
        Slot("Content", content)
        Slot("Actions", actions)
    }
}

class ExpanderRowNode(configuration: JetwaitaConfiguration) : SlotLayoutNode<ExpanderRow>(configuration) {
    override val widget = ExpanderRow()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        slot { widget.addPrefix(it) }
        slot { widget.addRow(it) }
        slot { widget.addAction(it) }
    }
}