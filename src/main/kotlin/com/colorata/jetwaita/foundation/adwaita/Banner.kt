package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.Banner
import org.gnome.gtk.Widget

@Composable
fun Banner(
    visible: Boolean,
    title: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {},
    buttonLabel: String = ""
) {
    ComposeGtkNode(modifier, factory = { BannerNode() }, update = {
        set(visible) { widget.revealed = it }
        set(title) { widget.title = it }
        set(onClick) { this.onClick = it }
        set(buttonLabel) { widget.buttonLabel = it }
    })
}

class BannerNode : AdwaitaNode<Banner>() {
    override val widget = Banner("Title")
    var onClick = {}

    override fun create(children: List<Widget>) {
        widget.onButtonClicked { onClick() }
    }
}