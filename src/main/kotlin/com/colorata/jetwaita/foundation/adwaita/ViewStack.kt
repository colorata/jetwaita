package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.ViewStack
import org.gnome.gtk.Widget

@Composable
fun ViewStack(state: ViewStackState, modifier: Modifier = Modifier, content: ViewSwitcherScope.() -> Unit) {
    val items = remember { itemsViewSwitcher(content) }
    ComposeGtkNode(
        modifier,
        factory = { ViewStackNode(state) },
        update = {
            set(items) { this.items = it }
        }
    ) {
        items.forEach {
            it.content()
        }
    }
}

class ViewStackNode(state: ViewStackState) : AdwaitaNode<ViewStack>() {
    override val widget = state.stack

    var items: List<ViewSwitcherItem> = listOf()

    override fun create(children: List<Widget>) {
        items.forEachIndexed { index, item ->
            val child = children[index]
            when (item) {
                is ViewSwitcherItem.Named -> widget.addNamed(child, item.name)
                is ViewSwitcherItem.Titled -> widget.addTitled(child, item.name, item.title)
                is ViewSwitcherItem.TitledWithIcon -> widget.addTitledWithIcon(
                    /* child = */ child,
                    /* name = */ item.name,
                    /* title = */ item.title,
                    /* iconName = */ item.icon
                )

                is ViewSwitcherItem.Widget -> widget.add(child)
            }
        }
    }
}