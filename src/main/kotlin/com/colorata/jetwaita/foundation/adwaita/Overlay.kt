package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slots
import org.gnome.gtk.Overlay

@Composable
fun Overlay(modifier: Modifier = Modifier, overlay: @Composable () -> Unit, content: @Composable () -> Unit) {
    ComposeGtkNode(modifier, factory = ::OverlayNode, update = {}) {
        Slot("Overlay", overlay)
        Slot("Content", content)
    }
}

class OverlayNode(configuration: JetwaitaConfiguration): SlotLayoutNode<Overlay>(configuration) {
    override val widget = Overlay()

    override val slots = slots {
        slot(onRemove = { widget.removeOverlay(it) }) { widget.addOverlay(it) }
        slot(onRemove = { widget.child = null }) { widget.child = it }
    }
}