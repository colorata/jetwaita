package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slots
import com.colorata.jetwaita.rememberWithConfiguration
import org.gnome.adw.PasswordEntryRow

@Composable
fun PasswordEntryRow(
    state: TextState,
    modifier: Modifier = Modifier,
    title: String = "",
    showApplyButton: Boolean = false,
    onApply: () -> Unit = {},
    prefix: @Composable () -> Unit = {},
    suffix: @Composable () -> Unit = {}
) {
    val node = rememberWithConfiguration(::PasswordEntryRowNode)
    LaunchedEffect(Unit) {
        state.provideEditable(node.widget)
        node.widget.onApply(onApply)
    }
    ComposeGtkNode(modifier, factory = { node }, update = {
        set(showApplyButton) { widget.showApplyButton = it }
        set(title) { widget.title = it }
    }) {
        Slot("Prefix", prefix)
        Slot("Suffix", suffix)
    }
}

class PasswordEntryRowNode(configuration: JetwaitaConfiguration): SlotLayoutNode<PasswordEntryRow>(configuration) {
    override val widget = PasswordEntryRow()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        slot { widget.addPrefix(it) }
        slot { widget.addSuffix(it) }
    }
}