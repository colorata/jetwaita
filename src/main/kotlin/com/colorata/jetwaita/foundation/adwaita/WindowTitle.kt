package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.WindowTitle

@Composable
fun WindowTitle(title: String, modifier: Modifier = Modifier, subtitle: String = "") {
    ComposeGtkNode(modifier = modifier, factory = { WindowTitleNode() }, update = {
        set(title) { widget.title = it }
        set(subtitle) { widget.subtitle = it }
    })
}

class WindowTitleNode: AdwaitaNode<WindowTitle>() {
    override val widget = WindowTitle("", "")
}