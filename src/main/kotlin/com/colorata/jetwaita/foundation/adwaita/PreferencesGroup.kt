package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.singleChildSlot
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slots
import org.gnome.adw.PreferencesGroup

@Composable
fun PreferencesGroup(
    title: String,
    modifier: Modifier = Modifier,
    description: String = "",
    suffix: @Composable () -> Unit = {},
    content: @Composable () -> Unit
) {
    ComposeGtkNode(modifier, factory = ::PreferencesGroupNode, update = {
        set(title) { widget.title = it }
        set(description) { widget.description = it }
    }) {
        Slot("Suffix", suffix)
        Slot("Content", content)
    }
}

class PreferencesGroupNode(configuration: JetwaitaConfiguration) : SlotLayoutNode<PreferencesGroup>(configuration) {
    override val widget = PreferencesGroup()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        singleChildSlot("PreferencesGroup suffix") { widget.headerSuffix = it }
        slot { widget.add(it) }
    }
}