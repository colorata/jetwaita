package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slots
import org.gnome.adw.ActionRow

@Composable
fun ActionRow(
    title: String,
    subtitle: String = "",
    modifier: Modifier = Modifier,
    prefix: @Composable () -> Unit = {},
    suffix: @Composable () -> Unit
) {
    ComposeGtkNode(modifier, factory = ::ActionRowNode, update = {
        set(title) { this.title = it }
        set(subtitle) { this.subtitle = it }
    }) {
        Slot("Prefix", prefix)
        Slot("Suffix", suffix)
    }
}

class ActionRowNode(configuration: JetwaitaConfiguration) : SlotLayoutNode<ActionRow>(configuration) {
    override val widget = ActionRow()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        slot { widget.addPrefix(it) }
        slot { widget.addSuffix(it) }
    }
    var title = ""
        set(value) {
            field = value
            widget.title = value
        }

    var subtitle = ""
        set(value) {
            field = value
            widget.subtitle = value
        }

}