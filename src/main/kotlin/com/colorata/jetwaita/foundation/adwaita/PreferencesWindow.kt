package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeWindowNode
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.foundation.WindowAction
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slotWithClass
import com.colorata.jetwaita.node.core.slots
import org.gnome.adw.PreferencesPage
import org.gnome.adw.PreferencesWindow

@Composable
fun PreferencesWindow(
    onExit: () -> WindowAction,
    modifier: Modifier = Modifier,
    canNavigateBack: Boolean = true,
    searchEnabled: Boolean = true,
    content: @Composable () -> Unit
) {
    ProvideToastManager {
        val manager = LocalToastManager.current
        ComposeWindowNode(
            modifier,
            factory = { _, config ->
                PreferencesWindowNode(config).apply {
                    manager.applier = ToastApplier { toast -> widget.addToast(toast) }
                }
            },
            update = {
                set(canNavigateBack) { widget.canNavigateBack = it }
                set(searchEnabled) { widget.searchEnabled = it }
            },
            onExit = onExit
        ) {
            Slot("Content", content)
        }
    }
}

class PreferencesWindowNode(configuration: JetwaitaConfiguration) :
    SlotLayoutNode<PreferencesWindow>(configuration) {
    override val widget = PreferencesWindow()

    override val slots = slots {
        slotWithClass<PreferencesPage>(onRemove = { widget.remove(it) }) {
            widget.add(it)
        }
    }

    override val includeInTree = false
}