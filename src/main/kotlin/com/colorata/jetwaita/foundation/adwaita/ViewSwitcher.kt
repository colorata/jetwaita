package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.ViewStack
import org.gnome.adw.ViewSwitcher
import org.gnome.adw.ViewSwitcherPolicy
import org.gnome.gtk.Widget

@Stable
interface ViewStackState {
    val stack: ViewStack
}

private class ViewStackStateImpl : ViewStackState {
    override val stack = ViewStack()
}

@Composable
fun rememberViewStackState(): ViewStackState =
    remember {
        ViewStackStateImpl()
    }

@Stable
interface ViewSwitcherScope {

    fun itemTitled(name: String, title: String, content: @Composable () -> Unit)

    fun itemNamed(name: String, content: @Composable () -> Unit)

    fun item(content: @Composable () -> Unit)

    fun itemTitledWithIcon(name: String, title: String, icon: String, content: @Composable () -> Unit)
}

internal fun itemsViewSwitcher(block: ViewSwitcherScope.() -> Unit): List<ViewSwitcherItem> {
    val items = mutableListOf<ViewSwitcherItem>()
    val scope = object : ViewSwitcherScope {
        override fun itemTitled(name: String, title: String, content: @Composable () -> Unit) {
            items.add(ViewSwitcherItem.Titled(name, title, content))
        }

        override fun itemNamed(name: String, content: @Composable () -> Unit) {
            items.add(ViewSwitcherItem.Named(name, content))
        }

        override fun itemTitledWithIcon(name: String, title: String, icon: String, content: @Composable () -> Unit) {
            items.add(ViewSwitcherItem.TitledWithIcon(name, title, icon, content))
        }

        override fun item(content: @Composable () -> Unit) {
            items.add(ViewSwitcherItem.Widget(content))
        }
    }
    scope.block()
    return items
}

sealed interface ViewSwitcherItem {
    val content: @Composable () -> Unit

    data class Titled(
        val name: String, val title: String, override val content: @Composable () -> Unit
    ) : ViewSwitcherItem

    data class Named(
        val name: String, override val content: @Composable () -> Unit
    ) : ViewSwitcherItem

    data class Widget(
        override val content: @Composable () -> Unit
    ) : ViewSwitcherItem

    data class TitledWithIcon(
        val name: String, val title: String, val icon: String, override val content: @Composable () -> Unit
    ) : ViewSwitcherItem
}

@Composable
fun ViewSwitcher(state: ViewStackState, modifier: Modifier = Modifier) {
    ComposeGtkNode(modifier, factory = { ViewSwitcherNode(state) }, update = {})
}

class ViewSwitcherNode(private val state: ViewStackState) : AdwaitaNode<ViewSwitcher>() {
    override val widget = ViewSwitcher()


    override fun create(children: List<Widget>) {
        widget.stack = state.stack
        widget.policy = ViewSwitcherPolicy.WIDE
    }

}