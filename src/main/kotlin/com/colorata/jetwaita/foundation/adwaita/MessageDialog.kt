package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeWindowNode
import com.colorata.jetwaita.foundation.WindowAction
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.MessageDialog
import org.gnome.adw.ResponseAppearance
import org.gnome.gtk.Widget
import org.gnome.gtk.Window

internal data class MessageDialogResponse(
    val label: String,
    val appearance: ResponseAppearance,
    val type: MessageDialogResponseType,
    val onClick: () -> Unit
)

enum class MessageDialogResponseType {
    Undefined,
    CloseResponse,
    DefaultResponse
}

interface MessageDialogResponseScope {
    fun response(
        label: String,
        appearance: ResponseAppearance = ResponseAppearance.DEFAULT,
        type: MessageDialogResponseType = MessageDialogResponseType.Undefined,
        onClick: () -> Unit = {}
    )
}

private fun messageDialogResponses(block: MessageDialogResponseScope.() -> Unit): List<MessageDialogResponse> {
    val responses = mutableListOf<MessageDialogResponse>()
    val scope = object : MessageDialogResponseScope {
        override fun response(
            label: String,
            appearance: ResponseAppearance,
            type: MessageDialogResponseType,
            onClick: () -> Unit
        ) {
            responses.add(MessageDialogResponse(label, appearance, type, onClick))
        }
    }
    scope.block()
    return responses
}

@Composable
fun MessageDialog(
    title: String,
    body: String,
    responses: MessageDialogResponseScope.() -> Unit = {},
    onExit: () -> WindowAction,
    modifier: Modifier = Modifier
) {
    ComposeWindowNode(
        modifier,
        factory = { parent, _ -> MessageDialogNode(parent) },
        update = {
            set(title) { this.heading = it }
            set(body) { this.body = it }
            set(responses) { this.responses = messageDialogResponses(it) }
        },
        onExit = {
            onExit()
        }
    ) {

    }
}


class MessageDialogNode(parent: Window) : AdwaitaNode<MessageDialog>() {
    override val widget by lazy {
        MessageDialog(parent, "", "")
    }

    override val includeInTree = false

    var heading = ""
        set(value) {
            field = value
            widget.heading = value
        }

    var body = ""
        set(value) {
            field = value
            widget.body = value
        }

    internal var responses = listOf<MessageDialogResponse>()

    override fun create(children: List<Widget>) {
        super.create(children)
        val responses = responses.toList()
        responses.forEach {
            widget.addResponse(it.label, it.label)
            widget.setResponseAppearance(it.label, it.appearance)
            when (it.type) {
                MessageDialogResponseType.CloseResponse -> widget.defaultResponse = it.label
                MessageDialogResponseType.DefaultResponse -> widget.closeResponse = it.label
                else -> {}
            }
        }
        widget.onResponse(null) { label ->
            responses.find { it.label == label }?.onClick?.invoke()
        }
    }
}