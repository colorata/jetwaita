package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.CarouselIndicatorDots
import org.gnome.adw.CarouselIndicatorLines

@Composable
fun CarouselIndicatorDots(state: CarouselState, modifier: Modifier = Modifier) {
    ComposeGtkNode(modifier, factory = { CarouselIndicatorDotsNode(state) }, update = {})
}

class CarouselIndicatorDotsNode(state: CarouselState): AdwaitaNode<CarouselIndicatorDots>() {
    override val widget = CarouselIndicatorDots().apply { carousel = state.carousel }
}

@Composable
fun CarouselIndicatorLines(state: CarouselState, modifier: Modifier = Modifier) {
    ComposeGtkNode(modifier, factory = { CarouselIndicatorLinesNode(state)}, update = {})
}

class CarouselIndicatorLinesNode(state: CarouselState): AdwaitaNode<CarouselIndicatorLines>() {
    override val widget = CarouselIndicatorLines().apply { carousel = state.carousel }
}