package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.Alignment
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Row
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.*
import org.gnome.adw.HeaderBar
import org.gnome.gtk.Widget

@Composable
fun HeaderBar(
    modifier: Modifier = Modifier,
    start: @Composable () -> Unit = {},
    end: @Composable () -> Unit = {},
    title: @Composable () -> Unit
) {
    ComposeGtkNode(
        modifier = modifier,
        factory = ::HeaderBarNode,
        update = {}
    ) {
        Slot("Start", start)
        Slot("Title", title)
        Slot("End", end)
    }
}

class HeaderBarNode(configuration: JetwaitaConfiguration) : SlotLayoutNode<HeaderBar>(configuration) {
    override val widget = HeaderBar()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        slot { widget.packStart(it) }
        singleChildSlot("HeaderBar title") { widget.titleWidget = it }
        slot { widget.packEnd(it) }
    }
}