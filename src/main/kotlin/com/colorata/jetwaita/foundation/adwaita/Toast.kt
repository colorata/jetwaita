package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SingleChildGtkNode
import org.gnome.adw.ToastOverlay
import org.gnome.gtk.Widget

interface ToastApplier {
    fun add(toast: Toast)

    fun remove(toast: Toast)
}

fun ToastApplier(add: (org.gnome.adw.Toast) -> Unit): ToastApplier {
    return object : ToastApplier {
        val toasts = mutableMapOf<Toast, org.gnome.adw.Toast>()
        override fun add(toast: Toast) {
            add(
                toast.copy(onDismiss = {
                    toasts.remove(toast)
                    toast.onDismiss()
                }).toGtkToast().also { gtkToast ->
                    toasts[toast] = gtkToast
                }
            )
        }

        override fun remove(toast: Toast) {
            toasts[toast]?.dismiss()
            toasts.remove(toast)
        }
    }
}

data class Toast(
    val title: String,
    val buttonLabel: String? = null,
    val priority: ToastPriority = ToastPriority.Normal,
    val onClick: () -> Unit = {},
    val onDismiss: () -> Unit = {}
)

enum class ToastPriority {
    High, Normal
}

class ToastManager {
    var applier: ToastApplier? = null

    fun add(toast: Toast) {
        applier?.add(toast)
    }

    fun remove(toast: Toast) {
        applier?.remove(toast)
    }
}

val LocalToastManager = compositionLocalOf<ToastManager> { error("No ToastManager is provided") }

fun ToastPriority.toGtkToastPriority(): org.gnome.adw.ToastPriority {
    return when (this) {
        ToastPriority.High -> org.gnome.adw.ToastPriority.HIGH
        ToastPriority.Normal -> org.gnome.adw.ToastPriority.NORMAL
    }
}

fun Toast.toGtkToast(): org.gnome.adw.Toast {
    return org.gnome.adw.Toast(title).apply {
        if (this@toGtkToast.buttonLabel != null) buttonLabel = this@toGtkToast.buttonLabel
        priority = this@toGtkToast.priority.toGtkToastPriority()
        onButtonClicked(onClick)
        onDismissed(onDismiss)
    }
}

@Composable
fun ProvideToastManager(content: @Composable () -> Unit) {
    val manager = remember { ToastManager() }
    CompositionLocalProvider(LocalToastManager provides manager) {
        content()
    }
}

@Composable
fun ToastOverlay(modifier: Modifier = Modifier, content: @Composable () -> Unit) {
    ProvideToastManager {
        val manager = LocalToastManager.current
        ComposeGtkNode(modifier, factory = {
            ToastOverlayNode(it).apply {
                manager.applier = ToastApplier { toast -> widget.addToast(toast) }
            }
        }, update = {}) {
            content()
        }
    }
}

class ToastOverlayNode(configuration: JetwaitaConfiguration) : SingleChildGtkNode<ToastOverlay>(configuration) {
    override val widget = ToastOverlay()

    override fun create(child: Widget) {
        widget.child = child
    }
}