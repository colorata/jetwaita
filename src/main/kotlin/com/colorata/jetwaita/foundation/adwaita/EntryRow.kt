package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Slot
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SlotLayoutNode
import com.colorata.jetwaita.node.core.slot
import com.colorata.jetwaita.node.core.slots
import com.colorata.jetwaita.rememberWithConfiguration
import org.gnome.adw.EntryRow
import org.gnome.gtk.Editable

@Stable
interface TextState {
    fun immediateText(): String

    fun edit(block: (String) -> String)

    fun provideText(block: () -> String)

    fun provideEditor(block: ((String) -> String) -> Unit)
}

fun TextState.provideEditable(editable: Editable) {
    provideText { editable.text }
    provideEditor { block ->
        val result = block(editable.text)
        if (result.isEmpty()) {
            editable.deleteText(0, editable.text.length)
        } else editable.text = result
    }
}

private class TextStateImpl(initial: String) : TextState {
    private var textProvider = { initial }

    private var editorProvider: ((String) -> String) -> Unit = {}
    override fun immediateText(): String {
        return textProvider()
    }

    override fun edit(block: (String) -> String) {
        editorProvider(block)
    }

    override fun provideText(block: () -> String) {
        textProvider = block
    }

    override fun provideEditor(block: ((String) -> String) -> Unit) {
        editorProvider = block
    }
}

fun TextState(initial: String): TextState = TextStateImpl(initial)

@Composable
fun rememberTextState(initial: String): TextState = remember { TextStateImpl(initial) }

@Composable
fun EntryRow(
    state: TextState,
    modifier: Modifier = Modifier,
    title: String = "",
    showApplyButton: Boolean = false,
    onApply: () -> Unit = {},
    prefix: @Composable () -> Unit = {},
    suffix: @Composable () -> Unit = {}
) {
    val node = rememberWithConfiguration(::EntryRowNode)
    LaunchedEffect(Unit) {
        state.provideEditable(node.widget)
        node.widget.onApply(onApply)
    }
    ComposeGtkNode(modifier, factory = { node }, update = {
        set(showApplyButton) { widget.showApplyButton = it }
        set(title) { widget.title = it }
    }) {
        Slot("Prefix", prefix)
        Slot("Suffix", suffix)
    }
}

class EntryRowNode(configuration: JetwaitaConfiguration): SlotLayoutNode<EntryRow>(configuration) {
    override val widget = EntryRow()

    override val slots = slots(onRemove = { widget.remove(it) }) {
        slot { widget.addPrefix(it) }
        slot { widget.addSuffix(it) }
    }
}