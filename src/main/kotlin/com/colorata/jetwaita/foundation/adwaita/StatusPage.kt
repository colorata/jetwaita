package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.foundation.Row
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import com.colorata.jetwaita.node.core.GtkNode
import com.colorata.jetwaita.node.core.SingleChildGtkNode
import org.gnome.adw.StatusPage
import org.gnome.gtk.Widget

@Composable
fun StatusPage(
    title: String,
    modifier: Modifier = Modifier,
    description: String = "",
    icon: String = "",
    content: @Composable () -> Unit = {}
) {
    ComposeGtkNode(modifier, factory = ::StatusPageNode, update = {
        set(icon) { this.icon = it }
        set(description) { this.description = it }
        set(title) { this.title = it }
    }) {
        Row {
            content()
        }
    }
}

class StatusPageNode(configuration: JetwaitaConfiguration) : SingleChildGtkNode<StatusPage>(configuration) {
    override val widget = StatusPage()

    var icon: String = ""
        set(value) {
            field = value
            widget.iconName = field
        }

    var title: String = ""
        set(value) {
            field = value
            widget.title = field
        }

    var description: String = ""
        set(value) {
            field = value
            widget.description = field
        }

    override fun create(child: Widget) {
        widget.child = child
    }
}