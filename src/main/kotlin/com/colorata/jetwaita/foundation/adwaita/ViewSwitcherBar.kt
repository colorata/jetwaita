package com.colorata.jetwaita.foundation.adwaita

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.ComposeGtkNode
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import org.gnome.adw.ViewSwitcherBar
import org.gnome.gtk.Widget

@Composable
fun ViewSwitcherBar(state: ViewStackState, modifier: Modifier = Modifier) {
    ComposeGtkNode(modifier, factory = { ViewSwitcherBarNode(state) }, update = {})
}

class ViewSwitcherBarNode(private val state: ViewStackState) : AdwaitaNode<ViewSwitcherBar>() {
    override val widget = ViewSwitcherBar()

    override fun create(children: List<Widget>) {
        widget.stack = state.stack
    }
}