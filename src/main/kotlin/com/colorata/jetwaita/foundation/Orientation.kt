package com.colorata.jetwaita.foundation

enum class Orientation {
    Horizontal,
    Vertical
}

fun Orientation.toGtkOrientation(): org.gnome.gtk.Orientation {
    return when (this) {
        Orientation.Horizontal -> org.gnome.gtk.Orientation.HORIZONTAL
        Orientation.Vertical -> org.gnome.gtk.Orientation.VERTICAL
    }
}