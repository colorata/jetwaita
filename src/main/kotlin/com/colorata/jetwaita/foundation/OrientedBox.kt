package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.modifier.semantics
import com.colorata.jetwaita.node.core.LayoutApplier
import com.colorata.jetwaita.node.core.LayoutNode
import com.colorata.jetwaita.node.core.WidgetSiblingLayoutApplier
import org.gnome.gtk.Box
import org.gnome.gtk.Widget

@Composable
internal fun OrientedBox(
    modifier: Modifier = Modifier,
    orientation: Orientation,
    horizontalAlignment: Alignment,
    verticalAlignment: Alignment,
    spacing: Int,
    content: @Composable () -> Unit
) {
    ComposeGtkNode(
        modifier = modifier,
        factory = { OrientedBoxNode() },
        update = {
            set(orientation) { this.orientation = it }
            set(horizontalAlignment) { this.horizontalAlignment = it }
            set(verticalAlignment) { this.verticalAlignment = it }
            set(spacing) { this.spacing = it }
        },
        content = content
    )
}

@Composable
fun Column(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment = Alignment.Start,
    verticalAlignment: Alignment = Alignment.Start,
    verticalSpacing: Int = 0,
    content: @Composable () -> Unit
) = OrientedBox(
    modifier = modifier.semantics("Column"),
    orientation = Orientation.Vertical,
    horizontalAlignment = horizontalAlignment,
    verticalAlignment = verticalAlignment,
    spacing = verticalSpacing,
    content = content
)

@Composable
fun Row(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment = Alignment.Start,
    verticalAlignment: Alignment = Alignment.Start,
    horizontalSpacing: Int = 0,
    content: @Composable () -> Unit
) = OrientedBox(
    modifier = modifier.semantics("Row"),
    orientation = Orientation.Horizontal,
    horizontalAlignment = horizontalAlignment,
    verticalAlignment = verticalAlignment,
    spacing = horizontalSpacing,
    content = content
)

class OrientedBoxNode : LayoutNode<Box>() {
    override val widget = Box(org.gnome.gtk.Orientation.VERTICAL, 0)

    var orientation = Orientation.Vertical
        set(value) {
            field = value
            widget.orientation = value.toGtkOrientation()
        }

    var horizontalAlignment = Alignment.Start
        set(value) {
            field = value
            widget.halign = value.toGtkAlignment()
        }

    var verticalAlignment = Alignment.Start
        set(value) {
            field = value
            widget.valign = value.toGtkAlignment()
        }

    var spacing = 0
        set(value) {
            field = value
            widget.spacing = value
        }

    override val layoutApplier = widget.asLayoutApplier()
}

fun Box.asLayoutApplier(): LayoutApplier = object : WidgetSiblingLayoutApplier {
    override fun append(widget: Widget) {
        this@asLayoutApplier.append(widget)
    }

    override fun insertChildAfter(widget: Widget, sibling: Widget?) {
        this@asLayoutApplier.insertChildAfter(widget, sibling)
    }

    override fun remove(widget: Widget) {
        this@asLayoutApplier.remove(widget)
    }
}