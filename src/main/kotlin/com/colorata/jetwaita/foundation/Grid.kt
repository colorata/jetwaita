package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.AdwaitaNode
import com.colorata.jetwaita.node.core.Change
import com.colorata.jetwaita.node.core.calculateDiff
import org.gnome.gtk.Grid
import org.gnome.gtk.Widget
import kotlin.math.max

interface GridScope {
    fun item(x: Int, y: Int, width: Int = 1, height: Int = 1, content: @Composable () -> Unit)
}

interface GridOrientedScope {
    fun item(width: Int = 1, height: Int = 1, content: @Composable () -> Unit)
}

fun GridOrientedScope.items(
    count: Int,
    width: (index: Int) -> Int = { 1 },
    height: (index: Int) -> Int = { 1 },
    content: @Composable (index: Int) -> Unit
) {
    repeat(count) { index ->
        item(width(index), height(index)) { content(index) }
    }
}

fun <T> GridOrientedScope.items(
    items: List<T>,
    width: (item: T) -> Int = { 1 },
    height: (item: T) -> Int = { 1 },
    content: @Composable (item: T) -> Unit
) {
    items.forEach { item ->
        item(width(item), height(item)) { content(item) }
    }
}

fun <T> GridOrientedScope.itemsIndexed(
    items: List<T>,
    width: (index: Int, item: T) -> Int = { _, _ -> 1 },
    height: (index: Int, item: T) -> Int = { _, _ -> 1 },
    content: @Composable (index: Int, item: T) -> Unit
) {
    items.forEachIndexed { index, item ->
        item(width(index, item), height(index, item)) { content(index, item) }
    }
}


fun GridScope.oriented(
    crossAxisStart: Int,
    mainAxisStart: Int = 0,
    orientation: Orientation,
    block: GridOrientedScope.() -> Unit
) {
    var progress = mainAxisStart
    val scope = object : GridOrientedScope {
        override fun item(width: Int, height: Int, content: @Composable () -> Unit) {
            if (orientation == Orientation.Horizontal)
                this@oriented.item(progress, crossAxisStart, width, height, content)
            else this@oriented.item(crossAxisStart, progress, width, height, content)
            progress += width
        }
    }
    scope.block()
}

fun GridScope.rowAt(y: Int, startX: Int = 0, block: GridOrientedScope.() -> Unit) {
    oriented(
        crossAxisStart = y,
        mainAxisStart = startX,
        orientation = Orientation.Horizontal,
        block = block
    )
}

fun GridScope.columnAt(x: Int, startY: Int = 0, block: GridOrientedScope.() -> Unit) {
    oriented(
        crossAxisStart = x,
        mainAxisStart = startY,
        orientation = Orientation.Vertical,
        block = block
    )
}

fun GridScope.flowOriented(
    crossAxisStart: Int,
    mainAxisStart: Int = 0,
    mainAxisWidth: Int,
    orientation: Orientation,
    block: GridOrientedScope.() -> Unit
) {
    var progress = mainAxisStart
    var progressCrossAxis = crossAxisStart
    var maxHeightInPrevious = 1
    val scope = object : GridOrientedScope {
        override fun item(width: Int, height: Int, content: @Composable () -> Unit) {
            println("BEFORE: $progress $progressCrossAxis")

            if (progress + width > mainAxisWidth) {
                progress = mainAxisStart
                progressCrossAxis += maxHeightInPrevious
            } else {
                maxHeightInPrevious = max(height, maxHeightInPrevious)
            }
            if (orientation == Orientation.Horizontal)
                this@flowOriented.item(progress, progressCrossAxis, width, height, content)
            else this@flowOriented.item(progressCrossAxis, progress, width, height, content)
            if (progress + width <= mainAxisWidth) {
                progress += width
            }
            println("AFTER: $progress $progressCrossAxis")
            println()
        }
    }
    scope.block()
}

fun GridScope.flowRowAt(y: Int, width: Int, startX: Int = 0, block: GridOrientedScope.() -> Unit) {
    flowOriented(
        crossAxisStart = y,
        mainAxisStart = startX,
        mainAxisWidth = width,
        orientation = Orientation.Horizontal,
        block = block
    )
}

fun GridScope.flowColumnAt(x: Int, height: Int, startY: Int = 0, block: GridOrientedScope.() -> Unit) {
    flowOriented(
        crossAxisStart = x,
        mainAxisStart = startY,
        mainAxisWidth = height,
        orientation = Orientation.Horizontal,
        block = block
    )
}

internal sealed interface GridItem {
    val x: Int
    val y: Int
    val width: Int
    val height: Int

    data class WithContent(
        override val x: Int,
        override val y: Int,
        override val width: Int,
        override val height: Int,
        val content: @Composable () -> Unit
    ) : GridItem

    data class WithWidget(
        override val x: Int,
        override val y: Int,
        override val width: Int,
        override val height: Int,
        val content: Widget
    ) : GridItem
}

internal fun gridScopeItems(block: GridScope.() -> Unit): List<GridItem.WithContent> {
    val items = mutableListOf<GridItem.WithContent>()
    val scope = object : GridScope {
        override fun item(x: Int, y: Int, width: Int, height: Int, content: @Composable () -> Unit) {
            items.add(
                GridItem.WithContent(
                    x = x,
                    y = y,
                    width = width,
                    height = height,
                    content = content
                )
            )
        }
    }
    scope.block()
    return items
}

@Composable
fun Grid(
    modifier: Modifier = Modifier,
    horizontalSpacing: Int = 0,
    verticalSpacing: Int = 0,
    block: GridScope.() -> Unit
) {
    val gridItems = gridScopeItems(block)
    ComposeGtkNode(modifier, factory = { GridNode() }, update = {
        set(block) { this.applyBlock = it }
        set(gridItems) { this.gridItems = it }
        set(horizontalSpacing) { this.horizontalSpacing = it }
        set(verticalSpacing) { this.verticalSpacing = it }
    }) {
        gridItems.forEach { it.content() }
    }
}

class GridNode : AdwaitaNode<Grid>() {
    override val widget = Grid()

    var applyBlock: GridScope.() -> Unit = {}

    var horizontalSpacing = 0
        set(value) {
            field = value
            widget.rowSpacing = value
        }

    var verticalSpacing = 0
        set(value) {
            field = value
            widget.columnSpacing = value
        }

    internal var gridItems: List<GridItem> = listOf()
    private var oldChildren = listOf<GridItem.WithWidget>()
    override fun render(children: List<Widget>): Widget {
        val itemsWithWidget = gridItems.mapIndexed { index, gridItem ->
            GridItem.WithWidget(
                x = gridItem.x,
                y = gridItem.y,
                width = gridItem.width,
                height = gridItem.height,
                content = children[index]
            )
        }
        val diff = calculateDiff(oldChildren, itemsWithWidget)
        diff.forEach { change ->
            when (change) {
                is Change.Add, is Change.Insert -> {
                    widget.attach(
                        /* child = */ change.content.content,
                        /* column = */ change.content.x,
                        /* row = */ change.content.y,
                        /* width = */ change.content.width,
                        /* height = */ change.content.height
                    )
                }

                is Change.Remove -> {
                    widget.remove(
                        /* child = */ change.content.content
                    )
                }
            }
        }
        oldChildren = itemsWithWidget
        return super.render(children)
    }
}