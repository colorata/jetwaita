package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.IndexSiblingLayoutApplier
import com.colorata.jetwaita.node.core.LayoutApplier
import com.colorata.jetwaita.node.core.LayoutNode
import org.gnome.gtk.FlowBox
import org.gnome.gtk.Widget

@Composable
internal fun FlowBox(
    modifier: Modifier,
    orientation: Orientation,
    horizontalAlignment: Alignment,
    verticalAlignment: Alignment,
    horizontalSpacing: Int,
    verticalSpacing: Int,
    childrenPerLineRange: IntRange?,
    content: @Composable () -> Unit
) {
    ComposeGtkNode(
        modifier = modifier,
        factory = { FlowBoxNode() },
        update = {
            set(horizontalAlignment) { this.horizontalAlignment = it }
            set(verticalAlignment) { this.verticalAlignment = it }
            set(horizontalSpacing) { this.horizontalSpacing = it }
            set(verticalSpacing) { this.verticalSpacing = it }
            set(orientation) { this.orientation = it }
            set(childrenPerLineRange) { this.childrenPerLineRange = it }

        }, content = content
    )
}

@Composable
fun FlowRow(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment = Alignment.Start,
    verticalAlignment: Alignment = Alignment.Start,
    horizontalSpacing: Int = 0,
    verticalSpacing: Int = 0,
    childrenPerLineRange: IntRange? = null,
    content: @Composable () -> Unit
) {
    FlowBox(
        modifier = modifier,
        orientation = Orientation.Horizontal,
        horizontalAlignment = horizontalAlignment,
        verticalAlignment = verticalAlignment,
        horizontalSpacing = horizontalSpacing,
        verticalSpacing = verticalSpacing,
        childrenPerLineRange = childrenPerLineRange,
        content = content
    )
}

@Composable
fun FlowColumn(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment = Alignment.Start,
    verticalAlignment: Alignment = Alignment.Start,
    horizontalSpacing: Int = 0,
    verticalSpacing: Int = 0,
    childrenPerLineRange: IntRange? = null,
    content: @Composable () -> Unit
) {
    FlowBox(
        modifier = modifier,
        orientation = Orientation.Vertical,
        horizontalAlignment = horizontalAlignment,
        verticalAlignment = verticalAlignment,
        horizontalSpacing = horizontalSpacing,
        verticalSpacing = verticalSpacing,
        childrenPerLineRange = childrenPerLineRange,
        content = content
    )
}

class FlowBoxNode : LayoutNode<FlowBox>() {
    override val widget = FlowBox()

    var horizontalAlignment = Alignment.Start
        set(value) {
            field = value
            widget.halign = value.toGtkAlignment()
        }

    var verticalAlignment = Alignment.Start
        set(value) {
            field = value
            widget.valign = value.toGtkAlignment()
        }

    var horizontalSpacing = 0
        set(value) {
            field = value
            widget.rowSpacing = value
        }

    var verticalSpacing = 0
        set(value) {
            field = value
            widget.columnSpacing = value
        }

    var orientation = Orientation.Horizontal
        set(value) {
            field = value
            widget.orientation = value.toGtkOrientation()
        }

    var childrenPerLineRange: IntRange? = null
        set(value) {
            field = value
            if (value != null) {
                widget.maxChildrenPerLine = value.last
                widget.minChildrenPerLine = value.first
            }
        }

    override val layoutApplier = widget.asLayoutApplier()
}

fun FlowBox.asLayoutApplier(): LayoutApplier = object : IndexSiblingLayoutApplier {
    override fun append(widget: Widget) {
        this@asLayoutApplier.append(widget)
    }

    override fun insertChildAfter(widget: Widget, sibling: Int) {
        this@asLayoutApplier.insert(widget, sibling)
    }

    override fun remove(widget: Widget) {
        this@asLayoutApplier.remove(widget)
    }

}