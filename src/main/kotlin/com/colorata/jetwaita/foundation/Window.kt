package com.colorata.jetwaita.foundation

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.node.core.SingleChildGtkNode
import org.gnome.adw.Window
import org.gnome.gtk.Widget

@Composable
fun Window(modifier: Modifier = Modifier, content: @Composable () -> Unit) {
    ComposeGtkNode(modifier, factory = ::WindowNode, update = {}, content = content)
}

class WindowNode(configuration: JetwaitaConfiguration) : SingleChildGtkNode<Window>(configuration) {
    override val widget by lazy {
        Window().apply {
            present()
        }
    }

    override fun create(child: Widget) {
        widget.content = child
    }
}