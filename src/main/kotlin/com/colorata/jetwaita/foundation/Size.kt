package com.colorata.jetwaita.foundation

data class Size(
    val width: Int,
    val height: Int
)