package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable

@Composable
fun Modifier.horizontalExpand(expand: Boolean = true) = widgetModifier(expand) {
    hexpand = expand
}

@Composable
fun Modifier.verticalExpand(expand: Boolean = true) = widgetModifier(expand) {
    vexpand = expand
}