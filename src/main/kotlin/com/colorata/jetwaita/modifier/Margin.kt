package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable

@Immutable
interface MarginValues {
    val top: Int
    val bottom: Int
    val start: Int
    val end: Int
}

internal class MarginValuesImpl(
    override val top: Int,
    override val bottom: Int,
    override val start: Int,
    override val end: Int
) : MarginValues


fun MarginValues(
    top: Int = 0,
    bottom: Int = 0,
    start: Int = 0,
    end: Int = 0
): MarginValues = MarginValuesImpl(
    top = top,
    bottom = bottom,
    start = start,
    end = end
)

fun MarginValues(
    horizontal: Int = 0,
    vertical: Int = 0
) = MarginValues(top = vertical, bottom = vertical, start = horizontal, end = horizontal)

fun MarginValues(all: Int = 0) = MarginValues(top = all, bottom = all, start = all, end = all)

@Composable
fun Modifier.margin(marginValues: MarginValues) = widgetModifier(marginValues) {
    marginTop = marginValues.top
    marginBottom = marginValues.bottom
    marginStart = marginValues.start
    marginEnd = marginValues.end
}

@Composable
fun Modifier.margin(
    top: Int = 0,
    bottom: Int = 0,
    start: Int = 0,
    end: Int = 0
) = margin(
    MarginValues(
        top = top,
        bottom = bottom,
        start = start,
        end = end
    )
)

@Composable
fun Modifier.margin(
    horizontal: Int = 0,
    vertical: Int = 0
) = margin(
    MarginValues(
        horizontal = horizontal,
        vertical = vertical
    )
)

@Composable
fun Modifier.margin(all: Int = 0) = margin(MarginValues(all))