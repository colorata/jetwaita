package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable

@Composable
fun Modifier.semantics(semantics: String) = modifier(semantics) {
    this.semantics = semantics
}