package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable

@Composable
fun Modifier.cssClass(name: String) = widgetModifier(name, onRemove = {
    if (it.isNotBlank()) removeCssClass(it)
}) {
    if (name.isNotBlank()) addCssClass(name)
}

@Composable
fun Modifier.name(name: String) = widgetModifier(name) {
    this.name = name
}