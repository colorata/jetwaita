package com.colorata.jetwaita.modifier

import androidx.compose.runtime.*
import com.colorata.jetwaita.node.core.GtkNode
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.runBlocking
import org.gnome.gtk.Widget

@Stable
interface Modifier {

    fun observe(): Flow<GtkNode<*>.() -> Unit>


    fun send(block: GtkNode<*>.() -> Unit)

    companion object : Modifier {
        private val flow = MutableSharedFlow<GtkNode<*>.() -> Unit>()
        override fun observe(): Flow<GtkNode<*>.() -> Unit> {
            return flow
        }

        override fun send(block: GtkNode<*>.() -> Unit) {
            runBlocking {
                flow.emit(block)
            }
        }
    }
}

fun Modifier.createModifier(): Modifier =
    object : Modifier {
        private val flow = MutableSharedFlow<GtkNode<*>.() -> Unit>(replay = 1)
        override fun observe(): Flow<GtkNode<*>.() -> Unit> {
            return merge(this@createModifier.observe(), flow)
        }

        override fun send(block: GtkNode<*>.() -> Unit) {
            runBlocking {
                flow.emit(block)
            }
        }
    }

@Composable
fun <K> Modifier.widgetModifier(key: K, onRemove: Widget.(oldKey: K) -> Unit = {}, block: Widget.() -> Unit): Modifier {
    return modifier(key, onRemove = { widget.onRemove(it) }, block = { widget.block() })
}

@Composable
fun <K> Modifier.modifier(
    key: K,
    onRemove: GtkNode<*>.(oldKey: K) -> Unit = {},
    block: GtkNode<*>.() -> Unit
): Modifier {
    val modifier by remember { mutableStateOf(createModifier()) }
    DisposableEffect(key) {
        modifier.send {
            block()
        }
        onDispose {
            modifier.send {
                onRemove(key)
            }
        }
    }
    return modifier
}
