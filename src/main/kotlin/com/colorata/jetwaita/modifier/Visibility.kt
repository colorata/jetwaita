package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable

@Composable
fun Modifier.visible(visible: Boolean) = widgetModifier(visible) {
    isVisible = visible
}

@Composable
fun Modifier.alpha(alpha: Float) = widgetModifier(alpha) {
    opacity = alpha.toDouble()
}