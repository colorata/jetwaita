package com.colorata.jetwaita.modifier

import androidx.compose.runtime.Composable
import com.colorata.jetwaita.foundation.Alignment
import com.colorata.jetwaita.foundation.toGtkAlignment

@Composable
fun Modifier.horizontalAlignment(alignment: Alignment) = widgetModifier(alignment) {
    halign = alignment.toGtkAlignment()
}

@Composable
fun Modifier.verticalAlignment(alignment: Alignment) = widgetModifier(alignment) {
    valign = alignment.toGtkAlignment()
}