package com.colorata.jetwaita.node.core

import org.gnome.gtk.Widget


abstract class GtkNode<T : Widget> {
    abstract val widget: T

    open val includeInTree: Boolean = true

    abstract fun render(children: List<Widget>): Widget


    open fun create(children: List<Widget>) {}


    open var semantics: String = this::class.simpleName?.removeSuffix("Node") ?: "GtkNode"

    val children = mutableListOf<GtkNode<*>>()

    private var initialized: Boolean = false

    private var oldRenderedChildren = listOf<Widget>()

    fun renderSafe(): Widget {
        if (!initialized) {
            children.map { it.renderSafe() }.apply {
                create(this)
            }
            initialized = true
        }

        // Rendering all widgets because some of them
        // that not included in tree (e.g. Window) can have children
        val includedInTree = children
            .map { it to it.renderSafe() }
            .filter { (node, _) -> node.includeInTree }
            .map { (_, widget) -> widget }

        return render(includedInTree)
    }
}

/**
 * Generates string representation of tree.
 * For exapmle:
 * ```
 * Column(Modifier.modifier(Unit) { println(createTreeOf { semantics }) }) {
 *     Button(onClick = {}, label = "+")
 *     Label("Count")
 *     Button(onClick = {}, label = "-")
 * }
 * ```
 * Will output this:
 * ```
 * Column
 * ├── Button
 * ├── Label
 * ╰── Button
 * ```
 */
fun GtkNode<*>.createTreeOf(block: GtkNode<*>.() -> Any?): String {
    val lines = createTreeOfLines(block)
    return buildString {
        lines.forEach { line ->
            append(line)
            append("\n")
        }
    }
}

private fun GtkNode<*>.createTreeOfLines(block: GtkNode<*>.() -> Any?): List<StringBuilder> {
    val result = mutableListOf<StringBuilder>()
    result.add(StringBuilder().append(block()))
    children.forEachIndexed { index, node ->
        val subtree = node.createTreeOfLines(block)
        if (index != children.lastIndex) {
            result.addSubtree(subtree)
        } else {
            result.addLastSubtree(subtree)
        }
    }
    return result
}

private fun MutableList<StringBuilder>.addSubtree(subtree: List<StringBuilder>) {
    val iterator = subtree.iterator()
    add(iterator.next().insert(0, "├── "))
    while (iterator.hasNext()) {
        add(iterator.next().insert(0, "│   "))
    }
}

private fun MutableList<StringBuilder>.addLastSubtree(subtree: List<StringBuilder>) {
    val iterator = subtree.iterator()
    add(iterator.next().insert(0, "╰── "))
    while (iterator.hasNext()) {
        add(iterator.next().insert(0, "    "))
    }
}