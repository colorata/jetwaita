package com.colorata.jetwaita.node.core

import org.gnome.gtk.Widget

sealed interface LayoutApplier {
    fun append(widget: Widget)
    fun remove(widget: Widget)
}

interface IndexSiblingLayoutApplier : LayoutApplier {
    fun insertChildAfter(widget: Widget, sibling: Int)
}

interface WidgetSiblingLayoutApplier : LayoutApplier {
    fun insertChildAfter(widget: Widget, sibling: Widget? = null)
}

interface NoInsertionLayoutApplier : LayoutApplier

internal fun LayoutApplier.applyChanges(changes: List<Change<Widget>>, children: List<Widget> = listOf()) {
    changes.forEach { change ->
        when (change) {
            is Change.Add -> {
                append(change.content)
            }

            is Change.Insert -> {
                if (this is IndexSiblingLayoutApplier) {
                    insertChildAfter(change.content, change.position + 1)
                } else if (this is WidgetSiblingLayoutApplier) {
                    val sibling = children[change.position]
                    insertChildAfter(change.content, sibling)
                }
            }

            is Change.Remove -> {
                remove(change.content)
            }
        }
    }
}