package com.colorata.jetwaita.node.core

import org.gnome.gtk.Widget

abstract class LayoutNode<T : Widget> : GtkNode<T>() {
    private var oldChildren = listOf<Widget>()
    private var oldRendered: Widget? = null

    abstract val layoutApplier: LayoutApplier

    override fun render(children: List<Widget>): Widget {
        val applier = layoutApplier
        val diff = calculateDiff(oldChildren, children)

        for (change in diff) {
            when (change) {
                is Change.Add -> {
                    applier.append(change.content)
                }

                is Change.Insert -> {
                    if (applier is IndexSiblingLayoutApplier) {
                        applier.insertChildAfter(change.content, change.position + 1)
                    } else if (applier is WidgetSiblingLayoutApplier) {
                        val sibling = children[change.position]
                        applier.insertChildAfter(change.content, sibling)
                    }
                }

                is Change.Remove -> {
                    applier.remove(change.content)
                }
            }
        }

        oldChildren = children.toList()
        oldRendered = widget
        return oldRendered ?: widget
    }
}

internal fun <T> calculateDiff(oldList: List<T>, newList: List<T>): List<Change<T>> {
    if (oldList == newList) return listOf()
    val diff = mutableListOf<Change<T>>()
    for (index in newList.indices) {
        if (newList[index] !in oldList) {
            if (index == 0) {
                diff.add(Change.Add(newList[index]))
            } else {
                diff.add(
                    Change.Insert(
                        content = newList[index],
                        position = index - 1
                    )
                )
            }
        }
    }
    for (index in oldList.indices) {
        if (oldList[index] !in newList) {
            diff.add(Change.Remove(oldList[index]))
        }
    }

    return diff
}

internal fun <T> calculateDiffWithoutInsertion(oldList: List<T>, newList: List<T>): List<Change<T>> {
    if (oldList == newList) return listOf()
    val diff = mutableListOf<Change<T>>()
    var breakIndex = 0

    for (index in 0..(minOf(oldList.lastIndex, newList.lastIndex))) {
        if (oldList[index] != newList[index]) {
            breakIndex = index
            break
        } else {
            breakIndex += 1
        }
    }

    (oldList.lastIndex downTo breakIndex).forEach { index ->
        diff.add(Change.Remove(oldList[index]))
    }
    (breakIndex..newList.lastIndex).forEach { index ->
        diff.add(Change.Add(newList[index]))
    }

    return diff
}

internal sealed interface Change<T> {
    val content: T

    data class Add<T>(override val content: T) : Change<T>

    data class Remove<T>(override val content: T) : Change<T>

    data class Insert<T>(override val content: T, val position: Int) : Change<T>
}

