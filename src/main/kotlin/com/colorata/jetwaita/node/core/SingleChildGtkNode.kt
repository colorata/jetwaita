package com.colorata.jetwaita.node.core

import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.requireSingleChild
import org.gnome.gtk.Widget

abstract class SingleChildGtkNode<T : Widget>(private val configuration: JetwaitaConfiguration) : GtkNode<T>() {
    open fun render(child: Widget): Widget {
        return widget
    }

    open fun create(child: Widget) {}
    override fun render(children: List<Widget>): Widget {
        configuration.requireSingleChild(children.size, semantics)
        return render(children.first())
    }

    override fun create(children: List<Widget>) {
        configuration.requireSingleChild(children.size, semantics)
        create(children.first())
    }
}