@file:OptIn(InternalJetwaitaApi::class)

package com.colorata.jetwaita.node.core

import com.colorata.jetwaita.InternalJetwaitaApi
import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.requireSingleChild
import org.gnome.gtk.Widget

abstract class SlotLayoutNode<T : Widget>(val configuration: JetwaitaConfiguration) : GtkNode<T>() {
    abstract val slots: List<LayoutApplier>

    private var oldChildren: List<List<Widget>>? = null

    override fun render(children: List<Widget>): Widget {
        if (oldChildren == null) oldChildren = List(slots.size) { listOf() }
        val allChildren = this.children.map { slot -> slot.children.map { it.renderSafe() } }
        if (allChildren != oldChildren) {
            slots.forEachIndexed { index, layoutApplier ->
                val currentChildren = allChildren[index]
                if (layoutApplier is NoInsertionLayoutApplier) {
                    val diff = calculateDiffWithoutInsertion(oldChildren!![index], currentChildren)
                    layoutApplier.applyChanges(diff)
                }
            }
        }
        oldChildren = allChildren
        return widget
    }
}

interface SlotsScope {
    @InternalJetwaitaApi
    fun add(block: (onRemove: ((Widget) -> Unit)?, configuration: JetwaitaConfiguration) -> LayoutApplier)
}

fun SlotsScope.slot(onRemove: ((Widget) -> Unit)? = null, append: (Widget) -> Unit) {
    add { remove, _ ->
        val currentRemove = remove ?: onRemove ?: {}
        object : NoInsertionLayoutApplier {
            override fun append(widget: Widget) {
                append(widget)
            }

            override fun remove(widget: Widget) {
                currentRemove(widget)
            }
        }
    }
}

inline fun <reified T : Widget> SlotsScope.slotWithClass(
    noinline onRemove: (T) -> Unit,
    noinline append: (T) -> Unit
) {
    val asT: Widget.() -> T = {
        require(this is T) { "Expected ${T::class.simpleName}, but got ${this::class.simpleName} instead" }
        this
    }
    slot(onRemove = {
        onRemove(it.asT())
    }) {
        append(it.asT())
    }
}

fun SlotsScope.singleChildSlot(semantics: String, set: (Widget?) -> Unit) {
    var appendsCount = 0
    add { _, configuration ->
        object : NoInsertionLayoutApplier {
            override fun append(widget: Widget) {
                appendsCount += 1
                configuration.requireSingleChild(appendsCount, semantics)
                set(widget)
            }

            override fun remove(widget: Widget) {
                appendsCount -= 1
                set(null)
            }
        }
    }
}

fun SlotsScope.indexSiblingSlot(
    onRemove: ((Widget) -> Unit)? = null, insert: (widget: Widget, sibling: Int) -> Unit, append: (Widget) -> Unit
) {
    add { remove, _ ->
        val currentRemove = remove ?: onRemove ?: {}
        object : IndexSiblingLayoutApplier {
            override fun append(widget: Widget) {
                append(widget)
            }

            override fun remove(widget: Widget) {
                currentRemove(widget)
            }

            override fun insertChildAfter(widget: Widget, sibling: Int) {
                insert(widget, sibling)
            }
        }
    }
}

fun SlotsScope.widgetSiblingSlot(
    onRemove: ((Widget) -> Unit)? = null, insert: (widget: Widget, sibling: Widget?) -> Unit, append: (Widget) -> Unit
) {
    add { remove, _ ->
        val currentRemove = remove ?: onRemove ?: {}
        object : WidgetSiblingLayoutApplier {
            override fun append(widget: Widget) {
                append(widget)
            }

            override fun remove(widget: Widget) {
                currentRemove(widget)
            }

            override fun insertChildAfter(widget: Widget, sibling: Widget?) {
                insert(widget, sibling)
            }
        }
    }
}

fun SlotLayoutNode<*>.slots(onRemove: ((Widget) -> Unit)? = null, block: SlotsScope.() -> Unit): List<LayoutApplier> {
    val slots = mutableListOf<LayoutApplier>()
    val scope = object : SlotsScope {
        override fun add(block: (onRemove: ((Widget) -> Unit)?, configuration: JetwaitaConfiguration) -> LayoutApplier) {
            slots.add(block(onRemove, configuration))
        }
    }
    scope.block()
    return slots
}