package com.colorata.jetwaita.node

import com.colorata.jetwaita.JetwaitaConfiguration
import com.colorata.jetwaita.foundation.Size
import com.colorata.jetwaita.node.core.SingleChildGtkNode
import org.gnome.adw.Application
import org.gnome.adw.ApplicationWindow
import org.gnome.gtk.Widget

class ApplicationWindowNode(app: Application, configuration: JetwaitaConfiguration) :
    SingleChildGtkNode<ApplicationWindow>(configuration) {
    override val widget: ApplicationWindow by lazy {
        ApplicationWindow(app).apply { present() }
    }

    var title = ""
        set(value) {
            field = value
            widget.title = value
        }

    var size = Size(0, 0)
        set(value) {
            field = value
            widget.setDefaultSize(size.width, size.height)
        }

    override fun render(child: Widget): Widget {
        return widget.apply {
            content = child
        }
    }
}