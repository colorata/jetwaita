package com.colorata.jetwaita.util

fun <T> List<T>.mutate(block: MutableList<T>.() -> Unit): List<T> {
    return toMutableList().apply(block).toList()
}