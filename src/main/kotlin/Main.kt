import androidx.compose.runtime.*
import com.colorata.jetwaita.foundation.*
import com.colorata.jetwaita.foundation.adwaita.*
import com.colorata.jetwaita.modifier.Modifier
import com.colorata.jetwaita.modifier.margin
import com.colorata.jetwaita.modifier.modifier
import com.colorata.jetwaita.node.core.createTreeOf
import com.colorata.jetwaita.runJetwaita
import com.colorata.jetwaita.window.GtkMain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runJetwaita(
    "com.colorata.jetwaita"
) {
    ToastOverlay {
        Column {
            val viewStackState = rememberViewStackState()
            HeaderBar {
                ViewSwitcher(viewStackState)
            }
            ViewStack(viewStackState) {
                itemTitled("Rows", "Rows") {
                    Rows()
                }
                itemTitled("Carousels", "Carousels") {
                    Carousels()
                }
                itemTitled("Hello3", "Hello3") {
                    Label("Hello3")
                }
                itemTitled("Hello4", "Hello4") {
                    Button(onClick = {}, label = "Hello4")
                }
            }
        }
    }
}

@Composable
fun Rows() {
    val mainWindowToasts = LocalToastManager.current
    var showEntryRow by remember { mutableStateOf(true) }
    Column(horizontalAlignment = Alignment.Fill) {
        val textState = rememberTextState("")
        var visible by remember { mutableStateOf(false) }
        Button(onClick = { visible = true }, label = "Show")
        if (visible) PreferencesWindow(onExit = {
            visible = false
            WindowAction.Close
        }) {
            val toastManager = LocalToastManager.current
            PreferencesPage {
                PreferencesGroup("Login page", Modifier.margin(10), description = "Please log in to system", suffix = {
                    Button(onClick = {
                        toastManager.add(Toast("Because needed"))
                        showEntryRow = !showEntryRow
                    }, "Why I am here?")
                }) {
                    if (showEntryRow) EntryRow(textState)

                    ExpanderRow("Login", subtitle = "Please login") {
                        val login = rememberTextState("Login")
                        EntryRow(login, title = "Login")

                        val password = rememberTextState("")
                        PasswordEntryRow(password, title = "Password", showApplyButton = true, onApply = {
                            toastManager.add(Toast("Logged in", "Undo", onClick = {
                                login.edit { "" }
                                password.edit { "" }
                            }))
                        })
                    }
                }
                PreferencesGroup("Logout page", Modifier.margin(10), description = "Please log in to system", suffix = {
                    Button(onClick = {
                        toastManager.add(Toast("Because needed"))
                        showEntryRow = !showEntryRow
                    }, "Why I am here?")
                }) {
                    if (showEntryRow) EntryRow(textState)

                    ExpanderRow("Login", subtitle = "Please login") {
                        val login = rememberTextState("Login")
                        EntryRow(login, title = "Login")

                        val password = rememberTextState("")
                        PasswordEntryRow(password, title = "Password", showApplyButton = true, onApply = {
                            toastManager.add(Toast("Logged in", "Undo", onClick = {
                                login.edit { "" }
                                password.edit { "" }
                            }))
                        })
                    }
                }
            }
            PreferencesPage {
                PreferencesGroup("Login page", Modifier.margin(10), description = "Please log in to system", suffix = {
                    Button(onClick = {
                        toastManager.add(Toast("Because needed"))
                        showEntryRow = !showEntryRow
                    }, "Why I am here?")
                }) {
                    if (showEntryRow) EntryRow(textState)

                    ExpanderRow("Login", subtitle = "Please login") {
                        val login = rememberTextState("Login")
                        EntryRow(login, title = "Login")

                        val password = rememberTextState("")
                        PasswordEntryRow(password, title = "Password", showApplyButton = true, onApply = {
                            toastManager.add(Toast("Logged in", "Undo", onClick = {
                                login.edit { "" }
                                password.edit { "" }
                            }))
                        })
                    }
                }
                PreferencesGroup("Logout page", Modifier.margin(10), description = "Please log in to system", suffix = {
                    Button(onClick = {
                        toastManager.add(Toast("Because needed"))
                        showEntryRow = !showEntryRow
                    }, "Why I am here?")
                }) {
                    if (showEntryRow) EntryRow(textState)

                    ExpanderRow("Login", subtitle = "Please login") {
                        val login = rememberTextState("Login")
                        EntryRow(login, title = "Login")

                        val password = rememberTextState("")
                        PasswordEntryRow(password, title = "Password", showApplyButton = true, onApply = {
                            toastManager.add(Toast("Logged in", "Undo", onClick = {
                                login.edit { "" }
                                password.edit { "" }
                            }))
                        })
                    }
                }
            }
        }
    }
}

@Composable
fun Carousels() {
    val manager = LocalToastManager.current
    val state = rememberCarouselState()
    Column(horizontalAlignment = Alignment.Fill) {
        Carousel(state) {
            repeat(5) {
                Button(onClick = { manager.add(Toast("Clicked $it element")) }, label = "$it element")
            }
        }
        CarouselIndicatorDots(state)
        CarouselIndicatorLines(state)
    }
}